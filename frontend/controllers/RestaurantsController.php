<?php

class RestaurantsController extends FController
{
	/*
	public function actions()
	{
	  return array(
		'captcha'=>array(
		'class'=>'CCaptchaAction',
		'width'=>'110',
		'height'=>'40',
		'maxLength' => 3,
		'minLength' => 2
		),
	  );
	}
	*/
	
	public function actionIndex()
	{
		
		$restorans = Restoran::model()->findAll();
		
		$model = new WriteUsForm();
		
		if(isset($_POST['WriteUsForm'])) {
			$model->attributes=$_POST['WriteUsForm'];

			$this->performAjaxValidation($model);
			
			
			$mailBlank = $this->renderPartial('//mailBlank/writeUs', ['data' => $model], true);
			
			$settings = new Settings();
			SendMail::send($settings->emailAdmin, "Сообщение с сайта Milimon", $mailBlank);
			
			echo CJSON::encode(
				1
			);
			Yii::app()->end();
		}
		
		$this->render("index", ['restorans' => $restorans, 'model' => $model]);
	}
	
}
