<?php
/**
 *
 * frontend.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
defined('APP_CONFIG_NAME') or define('APP_CONFIG_NAME', 'frontend');

// web application configuration
return array(
	'name' => '{APPLICATION NAME}',
	'basePath' => realPath(__DIR__ . '/..'),
	// path aliases
	'aliases' => array(
		'bootstrap' => dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiistrap',
		'yiiwheels' =>  dirname(__FILE__) . '/../..' . '/common/lib/vendor/2amigos/yiiwheels'
	),

	// application behaviors
	'behaviors' => array(),

	// controllers mappings
	'controllerMap' => array(),

	// application modules
	'modules' => array(

	),

	// application components
	'components' => array(

		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),

		'clientScript' => array(
			'scriptMap' => array(
				'bootstrap.min.css' => false,
				'bootstrap.min.js' => false,
				'bootstrap-yii.css' => false,
				'jquery.min.js' => false
			)
		),
		'urlManager' => array(
			'class'=>'UrlManager',
			// uncomment the following if you have enabled Apache's Rewrite module.
			'urlFormat' => 'path',
			'showScriptName' => false,

			'rules' => array(
			
				'(smr|ufa)/<controller:cart>/' => '<controller>/index',
				'(smr|ufa)/<controller:cart>/<action:(index|success)>' => '<controller>/<action>',
			
				'<controller:user>/<action:registrationDone>/<hash:\w+>' => '<controller>/<action>',
				'<controller:user>/<action:recoveryDone>/<hash:\w+>' => '<controller>/<action>',
				
				'<controller:menu>/<id:\d+>/<sort:\w+>' => '<controller>/index',
				'<controller:menu>/<id:\d+>' => '<controller>/index',
				
				'<controller:menu>/<action:index>/<id:\d+>/<sort:\w+>' => '<controller>/<action>',
				'<controller:menu>/<action:index>/<id:\d+>' => '<controller>/<action>',
				
				// default rules
				'<controller:\w+>/<id:\d+>' => '<controller>/index',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		'user' => array(
			'allowAutoLogin' => true,
		),
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
		'cache'=>array('class'=>'system.caching.CFileCache'),

        'curlRequest' => array(
            'class' => 'ext.curl.CurlRequest'
        )
	),
);
