<div class="content">
	
	<ul class="milimon-breadcrumb">
		<li>
			<a href="/">Milimon</a>
		</li>
		<li class="breadcrumbs-splitter"></li>
		<li>
			<a class="text-black">Вход</a>
		</li>
		<li class="breadcrumbs-splitter"></li>
	</ul>
	<div class="clearfix"></div>

	<div class="inner-page">
		<div class="main-h2">Вход</div>
		
		<div class="reg-form">
			
			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'login-form',
			    'enableClientValidation'=>true,
			    'clientOptions'=>array(
			        'validateOnSubmit'=>true,
			    ),
			)); ?>
			
			<div class="form-group">
				<label for="exampleInputEmail1">Email</label>
				<?php echo $form->textField($model,'email', array('class' => 'form-control')); ?>
			</div>

			<div class="form-group">
				<label for="exampleInputEmail1">Пароль</label>
				<?php echo $form->passwordField($model,'password', array('class' => 'form-control')); ?>
				<p class="control-label"><?php echo $form->error($model,'password'); ?></p>
			</div>
			
			<?php echo CHtml::htmlButton('Войти', array('class' => 'btn btn-default', 'type' => 'submit')); ?>
			<a href="<?php echo $this->createCPUUrl('/user/recovery/');?>" class="pull-right mt-5">Забыли пароль?</a>
			
			<?php $this->endWidget(); ?>
			
		</div>
		
	</div>
	
</div><!-- .content-->